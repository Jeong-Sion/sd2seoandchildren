/**
 * @author 길민희 and 박민지
 * @version 20131014
 * @param WordFrequencyCollection을 받아서 한 글자씩 분석
 * @return 단어 단위로 나눠진 WorldFrequencyCollection을 반환
 */

import java.io.*;
import java.util.ArrayList;

public class WordFrequencyAnalyzer {

	BufferedReader br=null;
	WordFrequencyCollection wordCol;
	String str="";
	char temp;

	public WordFrequencyAnalyzer(WordFrequencyCollection wordCol){
		this.wordCol = wordCol;
	}

	public void analyzeText(FileReader file) throws IOException{
		
		FileReader fi = file;
		
		br = new BufferedReader(fi);
		while(br.ready())
		{
			
			temp= (char)br.read();
			
			if(temp== ' ' || temp ==  '.' || temp==','){
				wordCol.add(str);
				str="";
			}
			else str+=temp;
			
		}
		wordCol.add(str);
		br.close();
	}

	public WordFrequencyCollection getResults(){
		return wordCol;
	}
}
